#include "hello-triangle.h"
#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <fcntl.h>
#include <limits.h>
#include <libgen.h>
#include <GL/glew.h>
#include <GL/gl.h>
#include <SDL2/SDL.h>

// shader program 
static GLuint helloTriangle;
// vertices buffer
static GLuint verticesBuffer;
// sdl window
static SDL_Window *window;
// sdl opengl context
static SDL_GLContext mainContext;
// sdl renderer
static SDL_Renderer *render;
// sdl initialized flag
static bool sdlInitialized;

/*
 * triangle vertices
 *
 * each line represents a 4d position of a vertex
 * ...clip space is 4d
 *
 */
static float vertexPositions[] = {
  0.75f, 0.75f, 0.0f, 1.0f,
  0.75f, -0.75f, 0.0f, 1.0f,
  -0.75f, -0.75f, 0.0f, 1.0f
};

/*
 * sdlErrExit
 *
 * print sdl error, quit sdl if initialized, and
 * exit program
 */
static void
sdlErrExit(const char *msg) {
  // print error
  fprintf(stderr, "%s: %s\n", msg, SDL_GetError());
  // check if sdl opengl context was initialized
  if (mainContext) {
    // destroy context
    SDL_GL_DeleteContext(mainContext);
  }
  // check if renderer was initialized
  if (render) { 
    // destroy renderer
    SDL_DestroyRenderer(render);
  }
  // check if sdl window was initialized
  if (window) {
    // destroy window
    SDL_DestroyWindow(window);
  }
  // check if sdl was initialized
  if (sdlInitialized) {
    // quit sdl
    SDL_Quit();
  }
  // exit
  exit(EXIT_FAILURE);
}

/*
 * getShaderDataStr
 *
 * output contents of shaderfile to a null terminated
 * char array
 */
static const GLchar *
getShaderDataStr(const char *shaderFile) {
  GLchar *dataStr = NULL;
  int fd = 0;

  // open file
  fd = open(shaderFile, FFLAGS);

  // check for errors
  if (fd < 0) {
    fprintf(stderr, "cannot open file for reading: %s\n", shaderFile);
    fprintf(stderr, "ERROR: %s\n", strerror(errno));
    return NULL;
  }

  // init buffer
  char *buf = calloc(1, SHADER_BUF_SIZE);
  size_t nbytes = SHADER_BUF_SIZE;
  ssize_t rbytes = 0;
  
  // read data to buffer
  rbytes = read(fd, buf, nbytes);

  // check for errors
  if (rbytes < 0) {
    fprintf(stderr, "cannot read file: %s\n", shaderFile);
    fprintf(stderr, "ERROR: %s\n", strerror(errno));
    return NULL;
  }

  // copy data
  int len = snprintf(NULL, 0, "%s", buf);
  dataStr = calloc(1, len+1);
  snprintf(dataStr, len+1, "%s", buf);

  // free memory
  free(buf);

  // close fd
  close(fd);

  return (const GLchar *)dataStr;
}

/*
 * createShader
 *
 * create opengl shader for vertex, geometry, or fragment
 */
static GLuint
createShader(GLenum shaderType, const char *shaderFile) {
  // get shader file contents
  const GLchar *shaderDataStr = getShaderDataStr(shaderFile);
  
  // create shader
  GLuint shader = glCreateShader(shaderType);
  // set shader source
  glShaderSource(shader, 1, &shaderDataStr, NULL);
  // compile shader
  glCompileShader(shader);
  
  // check for errors
  GLint status;
  glGetShaderiv(shader, GL_COMPILE_STATUS, &status);
  if (status == GL_FALSE) {
    // something went wrong, print error
    fprintf(stderr, "cannot compile shader: %s\n", shaderFile);
    // get shader log length
    int len, maxlen;
    glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &maxlen);
    // init log string
    char *log = calloc(1, maxlen);
    // get log
    glGetShaderInfoLog(shader, maxlen, &len, log);
    // print log
    fprintf(stderr, "ERROR: %s\n", log);
    // free memory
    free(log);
    // return 0
    return 0;
  }

  // free memory
  free((GLchar *)shaderDataStr);
  
  return shader;
}

/*
 * resizeViewport
 *
 * resize opengl viewport
 */
static void resizeViewport(int w, int h) {
  glViewport(0, 0, (GLsizei)w, (GLsizei)h); 
}

/*
 * initVertexBuffer
 *
 * copy triangle data to opengl buffer
 */
static void initVertexBuffer(void) {
  // init buffer object
  glGenBuffers(1, &verticesBuffer);

  // bind vertices buffer to gl_array_buffer binding target
  glBindBuffer(GL_ARRAY_BUFFER, verticesBuffer);

  // allocate memory for vertices buffer
  // copy data from vertex positions
  glBufferData(GL_ARRAY_BUFFER,
               sizeof(vertexPositions),
               vertexPositions,
               GL_STATIC_DRAW);

  // cleanup binding of buffer, unbind
  // todo: when to clean this up?
  glBindBuffer(GL_ARRAY_BUFFER, 0);
}

/*
 * display
 *
 * draw the triangle
 */
static void renderTriangle(void) {
  // set color to clear screen with (black)
  glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
  // clear screen. affect color buffer
  glClear(GL_COLOR_BUFFER_BIT);

  // set shader program to be used by all subsequent commands
  glUseProgram(helloTriangle);

  // init triangle state
  glBindBuffer(GL_ARRAY_BUFFER, verticesBuffer);
  glEnableVertexAttribArray(0);
  /*
   * tell opengl what format the buffer object has
   * 32bit float, 4 values per vertice, no space between values,
   * first value is beginning of buffer object
   */
  glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 0, 0);

  // render triangle
  glDrawArrays(GL_TRIANGLES, 0, 3);

  // cleanup some rendering work
  // todo: where to put this cleanup work?
  glDisableVertexAttribArray(0);
  glUseProgram(0);
}

/*
 * mainLoop
 *
 * use sdl2 to poll for keyboard events
 * loop until escape key is pressed down
 */
static void
mainLoop(void) {
  // event var
  SDL_Event event;
  // exit main loop condition
  bool gotesc = false;
  // use esc to quit
  while(!gotesc) {
    // force redraw
    SDL_GL_SwapWindow(window);
    // poll for event
    SDL_WaitEvent(&event);
    // check response
    switch (event.type) {
      // got a keydown event
    case SDL_KEYDOWN:
      // check virtual key code
      switch (event.key.keysym.sym) {
        // check for escape key
      case SDLK_ESCAPE:
        // got escape
        gotesc = true;
        break;
      }
      break;
    }
  }
}

/*
 * printVersions
 *
 * print opengl and glsl versions to std out
 */
static void
printVersions(void) { 
  // get opengl version
  const GLubyte *glv = glGetString(GL_VERSION);
  const GLubyte *glslv = glGetString(GL_SHADING_LANGUAGE_VERSION);
  // print
  printf("opengl version: %s\n", glv);
  printf("glsl version: %s\n", glslv);
}

/*
 * createSdlWindow
 *
 * create and sdl2 window
 */
static void
createSdlWindow(void) {
  // create sdl window
  window = SDL_CreateWindow(WINNAME,
                            SDL_WINDOWPOS_UNDEFINED,
                            SDL_WINDOWPOS_UNDEFINED,
                            WINWIDTH, WINHEIGHT,  SDL_WINDOW_OPENGL);
  // check for errors
  if (!window) {
    sdlErrExit("could not create sdl window");
  }
}

/*
 * initSdlGlAttrs
 *
 * set opengl attributes
 */
static void
initSdlGlAttrs(void) {
  // load default opengl library
  SDL_GL_LoadLibrary(NULL);
  // hardware or software rendering
  SDL_GL_SetAttribute(SDL_GL_ACCELERATED_VISUAL, HARDWARE_RENDERING);
  // request opengl version
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, OPENGL_MAJV);
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, OPENGL_MINV);
  // set context as core context by changing profile mask
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
  // set double buffered
  SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, DOUBLE_BUFFERED);
  // set bit depth
  SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, BITDEPTH);
}

/*
 * createSdlGlContext
 *
 * create an opengl context inside the sdl window
 */
static void
createSdlGlContext(void) {
  // create opengl context
  mainContext = SDL_GL_CreateContext(window);
  if (!mainContext) {
    sdlErrExit("cannot create opengl context");
  }
  // set as current context
  if (SDL_GL_MakeCurrent(window, mainContext) < 0) {
    sdlErrExit("cannot set current opengl context");
  }
}

/*
 * getBinaryPath
 *
 * get the absolute file path of the
 * current running binary
 */
static const char *
getBinaryDir(void) {
  // allocate memory for buffer
  char *buf = calloc(1, PATH_MAX);
  // copy path to buffer
  readlink("/proc/self/exe", buf, PATH_MAX);
  // get directory
  const char *dir = dirname(buf);
  // cast to const and return
  return dir;
}

/*
 * getShaderPath
 *
 * concatenate shader directory with
 * shader path for a relative path
 */
static const char *
getShaderPath(const char *file) {
  char *buf = NULL;
  // get directory of running binary
  const char *dir = getBinaryDir();
  // get length
  int len = snprintf(NULL, 0, "%s/%s/%s", dir, SHADER_DIR, file);
  // allocate memory for string
  buf = calloc(1, len+1);
  // copy tp buffer
  snprintf(buf, len+1, "%s/%s/%s", dir, SHADER_DIR, file);
  // free memory
  free((char *)dir);
  // cast to const and return
  return (const char *)buf;
}

/*
 * linkProgram
 *
 * link opengl program, print any errors
 *
 * return 0 on success
 * return -1 on error
 */
static int
linkProgram(GLuint program) {
  // link program
  glLinkProgram(program);

  // check for errors
  GLint linkErrCheck;
  glGetProgramiv(helloTriangle, GL_LINK_STATUS, &linkErrCheck);
  if (linkErrCheck != GL_TRUE) {
    // get shader log length
    int len, maxlen;
    glGetProgramiv(program, GL_INFO_LOG_LENGTH, &maxlen);
    // init log string
    char *log = calloc(1, maxlen);
    // get log
    glGetProgramInfoLog(program, maxlen, &len, log);
    // print log
    fprintf(stderr, "ERROR: %s\n", log);
    // free memory
    free(log);
    // return error
    return -1;
  }

  return 0;
}

/*
 * initGl
 *
 * innit opengl program
 */
static void initGl(void) {
  // create opengl program
  helloTriangle = glCreateProgram();

  // create vertex shader
  const char *vertFile = getShaderPath(VERT_FILE);
  GLuint vertexShader = createShader(GL_VERTEX_SHADER, vertFile);

  // free memory
  free((char *)vertFile);

  // check for errors
  if (!vertexShader) {
    sdlErrExit("cannot create vertex shader");
  }

  // attach vertex shader
  glAttachShader(helloTriangle, vertexShader);

  // create fragment shader
  const char *fragFile = getShaderPath(FRAG_FILE);
  GLuint fragmentShader = createShader(GL_FRAGMENT_SHADER, fragFile);

  // free memory
  free((char *)fragFile);
  
  if (!fragmentShader) {
    sdlErrExit("cannot create fragment shader");
  }

  // attach fragment shader
  glAttachShader(helloTriangle, fragmentShader);

  // link program
  if (linkProgram(helloTriangle) < 0) {
    sdlErrExit("cannot link program");
  }
  
  // init vertex buffer
  initVertexBuffer();
}

/*
 * initSdl
 *
 * initialize sdl for video
 */
static void
initSdl(void) {
  // init sdl
  if (SDL_Init(SDL_INIT_VIDEO) < 0) {
    sdlErrExit("cannot init sdl");
  }
  // set initialized flag
  sdlInitialized = true;
}

/*
 * initGlew
 *
 * initialize glew
 */
static void initGlew(void) {
  // todo: not sure if i want or need glew experimental
  glewExperimental = GL_TRUE;
  // init glew context
  if(glewInit() != GLEW_OK) {
    sdlErrExit("cannot initialize glew");
  }
}

/*
 * createSDLRender
 *
 * create sdl renderer
 */
static void createSDLRender(void) {
  // init renderer
  render = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
  // chekc for errors
  if (!render) {
    sdlErrExit("cannot create sdl renderer");
  } 
}

/*
 * setup
 *
 * initialize sdl, opengl, and glew
 */
static void
setup() {
  // init sdl
  initSdl();
  
  // create sdl window
  createSdlWindow();
  
  // init sdl opengl attributes
  initSdlGlAttrs();

  // init sdl renderer
  createSDLRender();
  
  // create opengl context in sdl window
  createSdlGlContext();
  
  // init glew
  initGlew();
  
  // print opengl/glsl versions
  printVersions();

  // todo: does this have to come after glewInit() ?
  // using vsync or not
  SDL_GL_SetSwapInterval(VSYNC);

  // init gl
  initGl();
}

/*
 * teardown
 *
 * destroy sdl window
 * quit sdl
 */
static void
teardown() {
  // destroy opengl context
  SDL_GL_DeleteContext(mainContext);
  // destroy renderer
  SDL_DestroyRenderer(render);
  // destroy window
  SDL_DestroyWindow(window);
  // shutdown sdl
  SDL_Quit();
}

int main(void) {
  // setup sdl/opengl/glew
  setup();
  
  // resize view port
  resizeViewport(WINWIDTH, WINHEIGHT);
  
  // render
  renderTriangle();
  
  // main loop
  mainLoop();

  // teardown
  teardown();
  
  exit(EXIT_SUCCESS);
}
