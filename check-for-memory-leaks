#!/bin/sh
## check for memory leaks using valgrind

# set usage string
usage="[[-c check] [-b binary] [-p \"binary params\"]]"
command_summary="
-c which valgrind check to run
-b which binary valgrind should use
-p binary params"

# args
check=""
checks="1 2"
bin=""
params=""

cmd_info() {
    printf '%s\n' "$0"
    printf '%s\n' "$usage"
    echo "$command_summary" | while read -r item; do
        printf '\t%s\n' "$item"
    done
}

# process options
while getopts hc:b:p: opt; do
    case "$opt" in
        c)
            check="$OPTARG"
            validcheckflag=1
            for i in $checks; do
                if [ "$check" = "$i" ]; then
                    validcheckflag=2
                    break
                fi
            done
            if [ "$validcheckflag" -eq 1 ]; then
                echo "ERROR: provided invalid valgrind check..."
                echo "valid options are: $checks"
                exit 1
            fi
            ;;
        b)
            bin="$OPTARG"
            if [ ! -f "$bin" ]; then
                echo "ERROR: file provided for option -b does not exist"
                echo "$bin"
                exit 1
            fi
            ;;
        p)
            params="$OPTARG"
            ;;
        h)
            cmd_info
            exit 0
            ;;
        *)
            cmd_info
            exit 1
    esac
done

# check all required options are provided
if [ -z "$check" ]; then
    echo "missing required options: -c"
    exit 1
fi

if [ -z "$bin" ]; then
    echo "missing required options: -b"
    exit 1
fi

if [ -z "$params" ]; then
    echo "WARNING: missing params"
fi

# echo vars
echo "$check $bin $params"

# run valgrind based on selected check
if [ "$check" = "1" ]; then
    eval valgrind --leak-check=full --show-leak-kinds=all \
             --track-origins=yes --verbose \
             "$bin $params"
elif [ "$check" = "2" ]; then
    eval valgrind --tool=memcheck --leak-check=yes --show-reachable=yes \
             --num-callers=20 --track-fds=yes --track-origins=yes \
             --verbose \
             "$bin $params"
fi

exit 0
