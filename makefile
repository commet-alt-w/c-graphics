.PHONY: release debug defualt clean-release clean-debug clean

build-dir = build
release-dir = release
debug-dir = debug

release:
	@cmake -S . -B $(build-dir)/$(release-dir) -DCMAKE_BUILD_TYPE=Release
	@cmake --build $(build-dir)/$(release-dir)

debug:
	@cmake -S . -B $(build-dir)/$(debug-dir) -DCMAKE_BUILD_TYPE=Debug
	@cmake --build $(build-dir)/$(debug-dir)

clean-release:
	@rm -rf $(build-dir)/$(release-dir)

clean-debug:
	@rm -rf $(build-dir)/$(debug-dir)

clean:
	@rm -rf $(build-dir)

default:
	release
