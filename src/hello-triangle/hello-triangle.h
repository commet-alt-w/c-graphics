#ifndef HELLO_TRIANGLE_H
#define HELLO_TRIANGLE_H

// opengl version
#define OPENGL_MAJV 4
#define OPENGL_MINV 5
// use software rendering (disable hardware rendering)
#define HARDWARE_RENDERING 0
// enable double buffered
#define DOUBLE_BUFFERED 1
// enable vsync
#define VSYNC 1
// bit depth
#define BITDEPTH 24
// window name
#define WINNAME "hello-triangle"
// window width/height
#define WINWIDTH 800
#define WINHEIGHT 600
// shader files
#define SHADER_DIR "shaders"
#define VERT_FILE "hello-triangle.vert"
#define FRAG_FILE "hello-triangle.frag"
// shader buffer size
#define SHADER_BUF_SIZE 1024
// file open flags
#define FFLAGS O_RDONLY 

#endif
