# c-graphics

graphics programming using sdl2/opengl/glew

## requirements

- `sdl2-dev`
- `glew-dev`

## building

- release
  - `$ make` or `$ make release`
- debug
  - `$ make debug`

## running

- release
  - `$ ./build/release/hello-triangle`
- debug
  - `$ ./build/release/hello-triangle`
- exit program
  - press `<esc>` key

## references

1. [OpenGL Wiki](https://www.khronos.org/opengl/wiki/)
1. [SDL Wiki](https://wiki.libsdl.org/)
1. [SDL and Modern OpenGL](https://lazyfoo.net/tutorials/SDL/51_SDL_and_modern_opengl/index.php)
1. [Learning Modern 3D Graphics Programming](https://www.cse.chalmers.se/edu/year/2018/course/TDA361/LearningModern3DGraphicsProgramming.pdf)
1. [If I use Glad and not GLEW will I miss on something?](https://stackoverflow.com/a/68823135/18280886)
1. [Creating an OpenGL 4.5 context using SDL2 and Glad](https://bcmpinc.wordpress.com/2015/08/18/creating-an-opengl-4-5-context-using-sdl2-and-glad/)

## license

[gnu gpl v3.0](/license)
